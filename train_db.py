##!/usr/bin/env python
'''
ABOUT: This simple program intend to train the data of Dittus-Boelter correaltions for the heat transfer (heating only). The data were provided in a 'DittusBoelterDatabase.csv'. In addition, a MATLAB script is also provided to generate data beyond the limits of Re and Pr considered here.
DEPENDS ON: Python3,XLRD, NumPy, Sklearn, Matplotlib, Pandas
DATE: 26.10.2018
AUTHOR: Sandeep Pandey (sandeep.pandey@ike.uni-stuttgart.de)
LINCENSE: GPL-3.0
'''
#Import relavant modules here
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn import metrics
from sklearn.neural_network import MLPRegressor

print('Reading the data from the CSV file')
data=pd.read_csv('DittusBoelterDatabase.csv')

#Split features and output
X= data.iloc[:,:-1].values
Y= data.iloc[:,2].values

#Split the all data in 80% (training) and 20% (testing)
X_train, X_test, Y_train, Y_test= train_test_split(X,Y,test_size=0.2,random_state=0)

print('Training the dataset')

#---Train the network---
#1. Linear regression
model=LinearRegression()
model.fit(X_train,Y_train)

#2. Random forest
modelRF=RandomForestRegressor(n_estimators=100,random_state=42) 
modelRF.fit(X_train,Y_train)

#3. Neuaral network
scaler = StandardScaler() #this will normalize the data
scaler.fit(X_train)
X_train_norm= scaler.transform(X_train)
X_test_norm= scaler.transform(X_test)
modelMLP = MLPRegressor(hidden_layer_sizes=(2,6,2),max_iter=1000)
modelMLP.fit(X_train_norm,Y_train)


#---Prediction test---
Y_Lin= model.predict(X_test)
Y_RF=modelRF.predict(X_test)
Y_MLP=modelMLP.predict(X_test_norm)
#out=pd.DataFrame({'Actual':Y_test,'LinRegression':Y_Lin,'RandomForest':Y_RF})

print('Mean Absolute Error of Linear Regression is:',metrics.mean_absolute_error(Y_test,Y_Lin))
print('Mean Absolute Error of Random Forest is:',metrics.mean_absolute_error(Y_test,Y_RF))
print('Mean Absolute Error of ANN is:',metrics.mean_absolute_error(Y_test,Y_MLP))

plt.plot(Y_test,Y_test,'k-')
plt.plot(Y_test,Y_Lin,'b*',Y_test,Y_RF,'g*',Y_test,Y_MLP,'r*')
plt.xlabel('$Nu_{exact}$',fontsize=18)
plt.ylabel('$Nu_{predicted}$',fontsize=18)
plt.show()
